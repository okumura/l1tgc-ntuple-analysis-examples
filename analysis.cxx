#include <stdlib.h>

#include "TagAndProbeAnalysisClass.h"
#include "AnalysisSkeleton.h"

#include <string>
#include <vector>
#include <fstream>

#include <TChain.h>

std::vector<std::string> ListOfFiles(std::string fileName);

int main() {
  const std::vector<std::string> files=ListOfFiles("input_files.txt");
  TChain myChain("physics");
  for (const auto file : files) { 
    if (file.empty()) { continue; }
    myChain.Add(file.c_str());
  }

  AnalysisSkeleton as(&myChain);
  as.Loop();
  
  TagAndProbeAnalysisClass tap(&myChain);
  tap.Loop();
  
  return EXIT_SUCCESS;
}


// utils
std::vector<std::string> ListOfFiles(std::string fileName)
{
  std::vector<std::string> rc;
  
  std::ifstream inputfile(fileName.c_str());
  char line[BUFSIZ];
  int counter = 0; 
  
  int iFile=0;
  while (not (inputfile.eof())) {
    inputfile.getline(line, sizeof(line));
    std::string lineStr(line);
    if (lineStr.empty()) {continue;}
    if (lineStr.substr(0, 1)=="#") {continue;}
    rc.push_back(line);
    iFile++;
  }
  return rc;
}
