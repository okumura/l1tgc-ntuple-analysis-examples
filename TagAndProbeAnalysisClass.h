//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Jul 20 11:39:53 2023 by ROOT version 6.22/08
// from TTree physics/Run-3 L1TGCNtuple
// found on file: user.hasegawa.34022423.EXT0._000068.root
//////////////////////////////////////////////////////////

#ifndef TagAndProbeAnalysisClass_h
#define TagAndProbeAnalysisClass_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TVector3.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

class TagAndProbeAnalysisClass {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           RunNumber;
   Int_t           EventNumber;
   Int_t           timeStamp;
   Int_t           timeStampNSOffset;
   Int_t           lbn;
   Int_t           bcid;
   Int_t           detmask0;
   Int_t           detmask1;
   Float_t         actualIntPerXing;
   Float_t         averageIntPerXing;
   Int_t           pixelFlags;
   Int_t           sctFlags;
   Int_t           trtFlags;
   Int_t           larFlags;
   Int_t           tileFlags;
   Int_t           muonFlags;
   Int_t           fwdFlags;
   Int_t           coreFlags;
   Int_t           pixelError;
   Int_t           sctError;
   Int_t           trtError;
   Int_t           larError;
   Int_t           tileError;
   Int_t           muonError;
   Int_t           fwdError;
   Int_t           coreError;
   std::vector<float>   *vxp_x;
   std::vector<float>   *vxp_y;
   std::vector<float>   *vxp_z;
   std::vector<float>   *vxp_cov_x;
   std::vector<float>   *vxp_cov_y;
   std::vector<float>   *vxp_cov_z;
   std::vector<float>   *vxp_cov_xy;
   std::vector<float>   *vxp_cov_xz;
   std::vector<float>   *vxp_cov_yz;
   std::vector<float>   *vxp_chi2;
   std::vector<int>     *vxp_ndof;
   std::vector<int>     *vxp_nTracks;
   std::vector<int>     *vxp_type;
   std::vector<float>   *TGC_prd_x;
   std::vector<float>   *TGC_prd_y;
   std::vector<float>   *TGC_prd_z;
   std::vector<float>   *TGC_prd_shortWidth;
   std::vector<float>   *TGC_prd_longWidth;
   std::vector<float>   *TGC_prd_length;
   std::vector<int>     *TGC_prd_isStrip;
   std::vector<int>     *TGC_prd_gasGap;
   std::vector<int>     *TGC_prd_channel;
   std::vector<int>     *TGC_prd_eta;
   std::vector<int>     *TGC_prd_phi;
   std::vector<int>     *TGC_prd_station;
   std::vector<int>     *TGC_prd_bunch;
   UInt_t          stgc_padtrigger_hit_n;
   UInt_t          stgc_padtrigger_pfeb_n;
   UInt_t          stgc_padtrigger_trigger_n;
   UInt_t          stgc_padtrigger_bcid_n;
   std::vector<unsigned int> *stgc_padtrigger_sourceid;
   std::vector<unsigned int> *stgc_padtrigger_flags;
   std::vector<unsigned int> *stgc_padtrigger_ec;
   std::vector<unsigned int> *stgc_padtrigger_fragid;
   std::vector<unsigned int> *stgc_padtrigger_secid;
   std::vector<unsigned int> *stgc_padtrigger_spare;
   std::vector<unsigned int> *stgc_padtrigger_orbit;
   std::vector<unsigned int> *stgc_padtrigger_bcid;
   std::vector<unsigned int> *stgc_padtrigger_l1id;
   std::vector<unsigned int> *stgc_padtrigger_hit_relbcid;
   std::vector<unsigned int> *stgc_padtrigger_hit_pfeb;
   std::vector<unsigned int> *stgc_padtrigger_hit_tdschannel;
   std::vector<unsigned int> *stgc_padtrigger_hit_padchannel;
   std::vector<unsigned int> *stgc_padtrigger_hit_sourceid;
   std::vector<unsigned int> *stgc_padtrigger_pfeb_addr;
   std::vector<unsigned int> *stgc_padtrigger_pfeb_nchan;
   std::vector<unsigned int> *stgc_padtrigger_pfeb_disconnected;
   std::vector<unsigned int> *stgc_padtrigger_pfeb_sourceid;
   std::vector<unsigned int> *stgc_padtrigger_trigger_bandid;
   std::vector<unsigned int> *stgc_padtrigger_trigger_phiid;
   std::vector<unsigned int> *stgc_padtrigger_trigger_relbcid;
   std::vector<unsigned int> *stgc_padtrigger_trigger_sourceid;
   std::vector<unsigned int> *stgc_padtrigger_bcid_rel;
   std::vector<unsigned int> *stgc_padtrigger_bcid_status;
   std::vector<unsigned int> *stgc_padtrigger_bcid_multzero;
   std::vector<unsigned int> *stgc_padtrigger_bcid_sourceid;
   std::vector<float>   *TGC_coin_x_In;
   std::vector<float>   *TGC_coin_y_In;
   std::vector<float>   *TGC_coin_z_In;
   std::vector<float>   *TGC_coin_x_Out;
   std::vector<float>   *TGC_coin_y_Out;
   std::vector<float>   *TGC_coin_z_Out;
   std::vector<float>   *TGC_coin_width_In;
   std::vector<float>   *TGC_coin_width_Out;
   std::vector<int>     *TGC_coin_isAside;
   std::vector<int>     *TGC_coin_isForward;
   std::vector<int>     *TGC_coin_phi;
   std::vector<int>     *TGC_coin_type;
   std::vector<int>     *TGC_coin_isStrip;
   std::vector<int>     *TGC_coin_sign;
   std::vector<int>     *TGC_coin_trackletId;
   std::vector<int>     *TGC_coin_trackletIdStrip;
   std::vector<int>     *TGC_coin_roi;
   std::vector<int>     *TGC_coin_pt;
   std::vector<int>     *TGC_coin_coinflag;
   std::vector<int>     *TGC_coin_innerflag;
   std::vector<int>     *TGC_coin_delta;
   std::vector<int>     *TGC_coin_sub;
   std::vector<int>     *TGC_coin_veto;
   std::vector<int>     *TGC_coin_bunch;
   std::vector<int>     *TGC_coin_inner;
   std::vector<int>     *TGC_coin_inner_bcid;
   std::vector<int>     *TGC_coin_inner_id;
   std::vector<int>     *TGC_coin_inner_eta;
   std::vector<int>     *TGC_coin_inner_phi;
   std::vector<int>     *TGC_coin_inner_deta;
   std::vector<int>     *TGC_coin_inner_dphi;
   std::vector<int>     *TGC_coin_inner_flag;
   std::vector<int>     *TGC_coin_inner_decision;
   std::vector<int>     *TGC_coin_inner_nsw_sl;
   std::vector<float>   *trig_L1_mu_eta;
   std::vector<float>   *trig_L1_mu_phi;
   std::vector<short>   *trig_L1_mu_thrNumber;
   std::vector<short>   *trig_L1_mu_RoINumber;
   std::vector<short>   *trig_L1_mu_sectorAddress;
   std::vector<short>   *trig_L1_mu_source;
   std::vector<short>   *trig_L1_mu_hemisphere;
   std::vector<short>   *trig_L1_mu_firstCandidate;
   std::vector<short>   *trig_L1_mu_moreCandInRPCPad;
   std::vector<short>   *trig_L1_mu_moreCandInSector;
   std::vector<short>   *trig_L1_mu_charge;
   std::vector<short>   *trig_L1_mu_BWcoin;
   std::vector<short>   *trig_L1_mu_InnerCoin;
   std::vector<short>   *trig_L1_mu_GoodMF;
   std::vector<short>   *trig_L1_mu_vetoed;
   std::vector<float>   *mu_pt;
   std::vector<float>   *mu_eta;
   std::vector<float>   *mu_phi;
   std::vector<float>   *mu_m;
   std::vector<int>     *mu_charge;
   std::vector<int>     *mu_author;
   std::vector<unsigned short> *mu_allAuthors;
   std::vector<int>     *mu_muonType;
   std::vector<float>   *mu_ptcone20;
   std::vector<float>   *mu_ptcone30;
   std::vector<float>   *mu_ptcone40;
   std::vector<float>   *mu_trackfitchi2;
   std::vector<float>   *mu_trackfitndof;
   std::vector<int>     *mu_isPassedMCP;
   std::vector<int>     *mu_quality;
   std::vector<std::vector<float> > *mu_ext_targetPlaneVec;
   std::vector<std::vector<float> > *mu_ext_targetEtaVec;
   std::vector<std::vector<float> > *mu_ext_targetPhiVec;
   std::vector<std::vector<float> > *mu_ext_targetPxVec;
   std::vector<std::vector<float> > *mu_ext_targetPyVec;
   std::vector<std::vector<float> > *mu_ext_targetPzVec;
   std::vector<std::string>  *trigger_info_chain;
   std::vector<std::vector<int> > *trigger_info_typeVec;
   std::vector<std::vector<float> > *trigger_info_ptVec;
   std::vector<std::vector<float> > *trigger_info_etaVec;
   std::vector<std::vector<float> > *trigger_info_phiVec;
   std::vector<int>     *TILE_murcv_trig_mod;
   std::vector<int>     *TILE_murcv_trig_part;
   std::vector<bool>    *TILE_murcv_trig_bit0;
   std::vector<bool>    *TILE_murcv_trig_bit1;
   std::vector<bool>    *TILE_murcv_trig_bit2;
   std::vector<bool>    *TILE_murcv_trig_bit3;

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_timeStamp;   //!
   TBranch        *b_timeStampNSOffset;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_detmask0;   //!
   TBranch        *b_detmask1;   //!
   TBranch        *b_actualIntPerXing;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_pixelFlags;   //!
   TBranch        *b_sctFlags;   //!
   TBranch        *b_trtFlags;   //!
   TBranch        *b_larFlags;   //!
   TBranch        *b_tileFlags;   //!
   TBranch        *b_muonFlags;   //!
   TBranch        *b_fwdFlags;   //!
   TBranch        *b_coreFlags;   //!
   TBranch        *b_pixelError;   //!
   TBranch        *b_sctError;   //!
   TBranch        *b_trtError;   //!
   TBranch        *b_larError;   //!
   TBranch        *b_tileError;   //!
   TBranch        *b_muonError;   //!
   TBranch        *b_fwdError;   //!
   TBranch        *b_coreError;   //!
   TBranch        *b_vxp_x;   //!
   TBranch        *b_vxp_y;   //!
   TBranch        *b_vxp_z;   //!
   TBranch        *b_vxp_cov_x;   //!
   TBranch        *b_vxp_cov_y;   //!
   TBranch        *b_vxp_cov_z;   //!
   TBranch        *b_vxp_cov_xy;   //!
   TBranch        *b_vxp_cov_xz;   //!
   TBranch        *b_vxp_cov_yz;   //!
   TBranch        *b_vxp_chi2;   //!
   TBranch        *b_vxp_ndof;   //!
   TBranch        *b_vxp_nTracks;   //!
   TBranch        *b_vxp_type;   //!
   TBranch        *b_TGC_prd_x;   //!
   TBranch        *b_TGC_prd_y;   //!
   TBranch        *b_TGC_prd_z;   //!
   TBranch        *b_TGC_prd_shortWidth;   //!
   TBranch        *b_TGC_prd_longWidth;   //!
   TBranch        *b_TGC_prd_length;   //!
   TBranch        *b_TGC_prd_isStrip;   //!
   TBranch        *b_TGC_prd_gasGap;   //!
   TBranch        *b_TGC_prd_channel;   //!
   TBranch        *b_TGC_prd_eta;   //!
   TBranch        *b_TGC_prd_phi;   //!
   TBranch        *b_TGC_prd_station;   //!
   TBranch        *b_TGC_prd_bunch;   //!
   TBranch        *b_stgc_padtrigger_hit_n;   //!
   TBranch        *b_stgc_padtrigger_pfeb_n;   //!
   TBranch        *b_stgc_padtrigger_trigger_n;   //!
   TBranch        *b_stgc_padtrigger_bcid_n;   //!
   TBranch        *b_stgc_padtrigger_sourceid;   //!
   TBranch        *b_stgc_padtrigger_flags;   //!
   TBranch        *b_stgc_padtrigger_ec;   //!
   TBranch        *b_stgc_padtrigger_fragid;   //!
   TBranch        *b_stgc_padtrigger_secid;   //!
   TBranch        *b_stgc_padtrigger_spare;   //!
   TBranch        *b_stgc_padtrigger_orbit;   //!
   TBranch        *b_stgc_padtrigger_bcid;   //!
   TBranch        *b_stgc_padtrigger_l1id;   //!
   TBranch        *b_stgc_padtrigger_hit_relbcid;   //!
   TBranch        *b_stgc_padtrigger_hit_pfeb;   //!
   TBranch        *b_stgc_padtrigger_hit_tdschannel;   //!
   TBranch        *b_stgc_padtrigger_hit_padchannel;   //!
   TBranch        *b_stgc_padtrigger_hit_sourceid;   //!
   TBranch        *b_stgc_padtrigger_pfeb_addr;   //!
   TBranch        *b_stgc_padtrigger_pfeb_nchan;   //!
   TBranch        *b_stgc_padtrigger_pfeb_disconnected;   //!
   TBranch        *b_stgc_padtrigger_pfeb_sourceid;   //!
   TBranch        *b_stgc_padtrigger_trigger_bandid;   //!
   TBranch        *b_stgc_padtrigger_trigger_phiid;   //!
   TBranch        *b_stgc_padtrigger_trigger_relbcid;   //!
   TBranch        *b_stgc_padtrigger_trigger_sourceid;   //!
   TBranch        *b_stgc_padtrigger_bcid_rel;   //!
   TBranch        *b_stgc_padtrigger_bcid_status;   //!
   TBranch        *b_stgc_padtrigger_bcid_multzero;   //!
   TBranch        *b_stgc_padtrigger_bcid_sourceid;   //!
   TBranch        *b_TGC_coin_x_In;   //!
   TBranch        *b_TGC_coin_y_In;   //!
   TBranch        *b_TGC_coin_z_In;   //!
   TBranch        *b_TGC_coin_x_Out;   //!
   TBranch        *b_TGC_coin_y_Out;   //!
   TBranch        *b_TGC_coin_z_Out;   //!
   TBranch        *b_TGC_coin_width_In;   //!
   TBranch        *b_TGC_coin_width_Out;   //!
   TBranch        *b_TGC_coin_isAside;   //!
   TBranch        *b_TGC_coin_isForward;   //!
   TBranch        *b_TGC_coin_phi;   //!
   TBranch        *b_TGC_coin_type;   //!
   TBranch        *b_TGC_coin_isStrip;   //!
   TBranch        *b_TGC_coin_sign;   //!
   TBranch        *b_TGC_coin_trackletId;   //!
   TBranch        *b_TGC_coin_trackletIdStrip;   //!
   TBranch        *b_TGC_coin_roi;   //!
   TBranch        *b_TGC_coin_pt;   //!
   TBranch        *b_TGC_coin_coinflag;   //!
   TBranch        *b_TGC_coin_innerflag;   //!
   TBranch        *b_TGC_coin_delta;   //!
   TBranch        *b_TGC_coin_sub;   //!
   TBranch        *b_TGC_coin_veto;   //!
   TBranch        *b_TGC_coin_bunch;   //!
   TBranch        *b_TGC_coin_inner;   //!
   TBranch        *b_TGC_coin_inner_bcid;   //!
   TBranch        *b_TGC_coin_inner_id;   //!
   TBranch        *b_TGC_coin_inner_eta;   //!
   TBranch        *b_TGC_coin_inner_phi;   //!
   TBranch        *b_TGC_coin_inner_deta;   //!
   TBranch        *b_TGC_coin_inner_dphi;   //!
   TBranch        *b_TGC_coin_inner_flag;   //!
   TBranch        *b_TGC_coin_inner_decision;   //!
   TBranch        *b_TGC_coin_inner_nsw_sl;   //!
   TBranch        *b_trig_L1_mu_eta;   //!
   TBranch        *b_trig_L1_mu_phi;   //!
   TBranch        *b_trig_L1_mu_thrNumber;   //!
   TBranch        *b_trig_L1_mu_RoINumber;   //!
   TBranch        *b_trig_L1_mu_sectorAddress;   //!
   TBranch        *b_trig_L1_mu_source;   //!
   TBranch        *b_trig_L1_mu_hemisphere;   //!
   TBranch        *b_trig_L1_mu_firstCandidate;   //!
   TBranch        *b_trig_L1_mu_moreCandInRPCPad;   //!
   TBranch        *b_trig_L1_mu_moreCandInSector;   //!
   TBranch        *b_trig_L1_mu_charge;   //!
   TBranch        *b_trig_L1_mu_BWcoin;   //!
   TBranch        *b_trig_L1_mu_InnerCoin;   //!
   TBranch        *b_trig_L1_mu_GoodMF;   //!
   TBranch        *b_trig_L1_mu_vetoed;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_m;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_author;   //!
   TBranch        *b_mu_allAuthors;   //!
   TBranch        *b_mu_muonType;   //!
   TBranch        *b_mu_ptcone20;   //!
   TBranch        *b_mu_ptcone30;   //!
   TBranch        *b_mu_ptcone40;   //!
   TBranch        *b_mu_trackfitchi2;   //!
   TBranch        *b_mu_trackfitndof;   //!
   TBranch        *b_mu_isPassedMCP;   //!
   TBranch        *b_mu_quality;   //!
   TBranch        *b_mu_ext_targetPlaneVec;   //!
   TBranch        *b_mu_ext_targetEtaVec;   //!
   TBranch        *b_mu_ext_targetPhiVec;   //!
   TBranch        *b_mu_ext_targetPxVec;   //!
   TBranch        *b_mu_ext_targetPyVec;   //!
   TBranch        *b_mu_ext_targetPzVec;   //!
   TBranch        *b_trigger_info_chain;   //!
   TBranch        *b_trigger_info_typeVec;   //!
   TBranch        *b_trigger_info_ptVec;   //!
   TBranch        *b_trigger_info_etaVec;   //!
   TBranch        *b_trigger_info_phiVec;   //!
   TBranch        *b_TILE_murcv_trig_mod;   //!
   TBranch        *b_TILE_murcv_trig_part;   //!
   TBranch        *b_TILE_murcv_trig_bit0;   //!
   TBranch        *b_TILE_murcv_trig_bit1;   //!
   TBranch        *b_TILE_murcv_trig_bit2;   //!
   TBranch        *b_TILE_murcv_trig_bit3;   //!

   TagAndProbeAnalysisClass(TTree *tree=0);
   virtual ~TagAndProbeAnalysisClass();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

  void RetrieveTriggerObjects(const std::vector<std::string>& chainName, std::vector<TVector3>& trig_objects);
  
};

#endif

#ifdef TagAndProbeAnalysisClass_cxx
TagAndProbeAnalysisClass::TagAndProbeAnalysisClass(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("user.hasegawa.34022423.EXT0._000068.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("user.hasegawa.34022423.EXT0._000068.root");
      }
      f->GetObject("physics",tree);

   }
   Init(tree);
}

TagAndProbeAnalysisClass::~TagAndProbeAnalysisClass()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t TagAndProbeAnalysisClass::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t TagAndProbeAnalysisClass::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void TagAndProbeAnalysisClass::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   vxp_x = 0;
   vxp_y = 0;
   vxp_z = 0;
   vxp_cov_x = 0;
   vxp_cov_y = 0;
   vxp_cov_z = 0;
   vxp_cov_xy = 0;
   vxp_cov_xz = 0;
   vxp_cov_yz = 0;
   vxp_chi2 = 0;
   vxp_ndof = 0;
   vxp_nTracks = 0;
   vxp_type = 0;
   TGC_prd_x = 0;
   TGC_prd_y = 0;
   TGC_prd_z = 0;
   TGC_prd_shortWidth = 0;
   TGC_prd_longWidth = 0;
   TGC_prd_length = 0;
   TGC_prd_isStrip = 0;
   TGC_prd_gasGap = 0;
   TGC_prd_channel = 0;
   TGC_prd_eta = 0;
   TGC_prd_phi = 0;
   TGC_prd_station = 0;
   TGC_prd_bunch = 0;
   stgc_padtrigger_sourceid = 0;
   stgc_padtrigger_flags = 0;
   stgc_padtrigger_ec = 0;
   stgc_padtrigger_fragid = 0;
   stgc_padtrigger_secid = 0;
   stgc_padtrigger_spare = 0;
   stgc_padtrigger_orbit = 0;
   stgc_padtrigger_bcid = 0;
   stgc_padtrigger_l1id = 0;
   stgc_padtrigger_hit_relbcid = 0;
   stgc_padtrigger_hit_pfeb = 0;
   stgc_padtrigger_hit_tdschannel = 0;
   stgc_padtrigger_hit_padchannel = 0;
   stgc_padtrigger_hit_sourceid = 0;
   stgc_padtrigger_pfeb_addr = 0;
   stgc_padtrigger_pfeb_nchan = 0;
   stgc_padtrigger_pfeb_disconnected = 0;
   stgc_padtrigger_pfeb_sourceid = 0;
   stgc_padtrigger_trigger_bandid = 0;
   stgc_padtrigger_trigger_phiid = 0;
   stgc_padtrigger_trigger_relbcid = 0;
   stgc_padtrigger_trigger_sourceid = 0;
   stgc_padtrigger_bcid_rel = 0;
   stgc_padtrigger_bcid_status = 0;
   stgc_padtrigger_bcid_multzero = 0;
   stgc_padtrigger_bcid_sourceid = 0;
   TGC_coin_x_In = 0;
   TGC_coin_y_In = 0;
   TGC_coin_z_In = 0;
   TGC_coin_x_Out = 0;
   TGC_coin_y_Out = 0;
   TGC_coin_z_Out = 0;
   TGC_coin_width_In = 0;
   TGC_coin_width_Out = 0;
   TGC_coin_isAside = 0;
   TGC_coin_isForward = 0;
   TGC_coin_phi = 0;
   TGC_coin_type = 0;
   TGC_coin_isStrip = 0;
   TGC_coin_sign = 0;
   TGC_coin_trackletId = 0;
   TGC_coin_trackletIdStrip = 0;
   TGC_coin_roi = 0;
   TGC_coin_pt = 0;
   TGC_coin_coinflag = 0;
   TGC_coin_innerflag = 0;
   TGC_coin_delta = 0;
   TGC_coin_sub = 0;
   TGC_coin_veto = 0;
   TGC_coin_bunch = 0;
   TGC_coin_inner = 0;
   TGC_coin_inner_bcid = 0;
   TGC_coin_inner_id = 0;
   TGC_coin_inner_eta = 0;
   TGC_coin_inner_phi = 0;
   TGC_coin_inner_deta = 0;
   TGC_coin_inner_dphi = 0;
   TGC_coin_inner_flag = 0;
   TGC_coin_inner_decision = 0;
   TGC_coin_inner_nsw_sl = 0;
   trig_L1_mu_eta = 0;
   trig_L1_mu_phi = 0;
   trig_L1_mu_thrNumber = 0;
   trig_L1_mu_RoINumber = 0;
   trig_L1_mu_sectorAddress = 0;
   trig_L1_mu_source = 0;
   trig_L1_mu_hemisphere = 0;
   trig_L1_mu_firstCandidate = 0;
   trig_L1_mu_moreCandInRPCPad = 0;
   trig_L1_mu_moreCandInSector = 0;
   trig_L1_mu_charge = 0;
   trig_L1_mu_BWcoin = 0;
   trig_L1_mu_InnerCoin = 0;
   trig_L1_mu_GoodMF = 0;
   trig_L1_mu_vetoed = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_m = 0;
   mu_charge = 0;
   mu_author = 0;
   mu_allAuthors = 0;
   mu_muonType = 0;
   mu_ptcone20 = 0;
   mu_ptcone30 = 0;
   mu_ptcone40 = 0;
   mu_trackfitchi2 = 0;
   mu_trackfitndof = 0;
   mu_isPassedMCP = 0;
   mu_quality = 0;
   mu_ext_targetPlaneVec = 0;
   mu_ext_targetEtaVec = 0;
   mu_ext_targetPhiVec = 0;
   mu_ext_targetPxVec = 0;
   mu_ext_targetPyVec = 0;
   mu_ext_targetPzVec = 0;
   trigger_info_chain = 0;
   trigger_info_typeVec = 0;
   trigger_info_ptVec = 0;
   trigger_info_etaVec = 0;
   trigger_info_phiVec = 0;
   TILE_murcv_trig_mod = 0;
   TILE_murcv_trig_part = 0;
   TILE_murcv_trig_bit0 = 0;
   TILE_murcv_trig_bit1 = 0;
   TILE_murcv_trig_bit2 = 0;
   TILE_murcv_trig_bit3 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_runNumber);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("timeStamp", &timeStamp, &b_timeStamp);
   fChain->SetBranchAddress("timeStampNSOffset", &timeStampNSOffset, &b_timeStampNSOffset);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("detmask0", &detmask0, &b_detmask0);
   fChain->SetBranchAddress("detmask1", &detmask1, &b_detmask1);
   fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
   fChain->SetBranchAddress("pixelFlags", &pixelFlags, &b_pixelFlags);
   fChain->SetBranchAddress("sctFlags", &sctFlags, &b_sctFlags);
   fChain->SetBranchAddress("trtFlags", &trtFlags, &b_trtFlags);
   fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
   fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
   fChain->SetBranchAddress("muonFlags", &muonFlags, &b_muonFlags);
   fChain->SetBranchAddress("fwdFlags", &fwdFlags, &b_fwdFlags);
   fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
   fChain->SetBranchAddress("pixelError", &pixelError, &b_pixelError);
   fChain->SetBranchAddress("sctError", &sctError, &b_sctError);
   fChain->SetBranchAddress("trtError", &trtError, &b_trtError);
   fChain->SetBranchAddress("larError", &larError, &b_larError);
   fChain->SetBranchAddress("tileError", &tileError, &b_tileError);
   fChain->SetBranchAddress("muonError", &muonError, &b_muonError);
   fChain->SetBranchAddress("fwdError", &fwdError, &b_fwdError);
   fChain->SetBranchAddress("coreError", &coreError, &b_coreError);
   fChain->SetBranchAddress("vxp_x", &vxp_x, &b_vxp_x);
   fChain->SetBranchAddress("vxp_y", &vxp_y, &b_vxp_y);
   fChain->SetBranchAddress("vxp_z", &vxp_z, &b_vxp_z);
   fChain->SetBranchAddress("vxp_cov_x", &vxp_cov_x, &b_vxp_cov_x);
   fChain->SetBranchAddress("vxp_cov_y", &vxp_cov_y, &b_vxp_cov_y);
   fChain->SetBranchAddress("vxp_cov_z", &vxp_cov_z, &b_vxp_cov_z);
   fChain->SetBranchAddress("vxp_cov_xy", &vxp_cov_xy, &b_vxp_cov_xy);
   fChain->SetBranchAddress("vxp_cov_xz", &vxp_cov_xz, &b_vxp_cov_xz);
   fChain->SetBranchAddress("vxp_cov_yz", &vxp_cov_yz, &b_vxp_cov_yz);
   fChain->SetBranchAddress("vxp_chi2", &vxp_chi2, &b_vxp_chi2);
   fChain->SetBranchAddress("vxp_ndof", &vxp_ndof, &b_vxp_ndof);
   fChain->SetBranchAddress("vxp_nTracks", &vxp_nTracks, &b_vxp_nTracks);
   fChain->SetBranchAddress("vxp_type", &vxp_type, &b_vxp_type);
   fChain->SetBranchAddress("TGC_prd_x", &TGC_prd_x, &b_TGC_prd_x);
   fChain->SetBranchAddress("TGC_prd_y", &TGC_prd_y, &b_TGC_prd_y);
   fChain->SetBranchAddress("TGC_prd_z", &TGC_prd_z, &b_TGC_prd_z);
   fChain->SetBranchAddress("TGC_prd_shortWidth", &TGC_prd_shortWidth, &b_TGC_prd_shortWidth);
   fChain->SetBranchAddress("TGC_prd_longWidth", &TGC_prd_longWidth, &b_TGC_prd_longWidth);
   fChain->SetBranchAddress("TGC_prd_length", &TGC_prd_length, &b_TGC_prd_length);
   fChain->SetBranchAddress("TGC_prd_isStrip", &TGC_prd_isStrip, &b_TGC_prd_isStrip);
   fChain->SetBranchAddress("TGC_prd_gasGap", &TGC_prd_gasGap, &b_TGC_prd_gasGap);
   fChain->SetBranchAddress("TGC_prd_channel", &TGC_prd_channel, &b_TGC_prd_channel);
   fChain->SetBranchAddress("TGC_prd_eta", &TGC_prd_eta, &b_TGC_prd_eta);
   fChain->SetBranchAddress("TGC_prd_phi", &TGC_prd_phi, &b_TGC_prd_phi);
   fChain->SetBranchAddress("TGC_prd_station", &TGC_prd_station, &b_TGC_prd_station);
   fChain->SetBranchAddress("TGC_prd_bunch", &TGC_prd_bunch, &b_TGC_prd_bunch);
   fChain->SetBranchAddress("stgc_padtrigger_hit_n", &stgc_padtrigger_hit_n, &b_stgc_padtrigger_hit_n);
   fChain->SetBranchAddress("stgc_padtrigger_pfeb_n", &stgc_padtrigger_pfeb_n, &b_stgc_padtrigger_pfeb_n);
   fChain->SetBranchAddress("stgc_padtrigger_trigger_n", &stgc_padtrigger_trigger_n, &b_stgc_padtrigger_trigger_n);
   fChain->SetBranchAddress("stgc_padtrigger_bcid_n", &stgc_padtrigger_bcid_n, &b_stgc_padtrigger_bcid_n);
   fChain->SetBranchAddress("stgc_padtrigger_sourceid", &stgc_padtrigger_sourceid, &b_stgc_padtrigger_sourceid);
   fChain->SetBranchAddress("stgc_padtrigger_flags", &stgc_padtrigger_flags, &b_stgc_padtrigger_flags);
   fChain->SetBranchAddress("stgc_padtrigger_ec", &stgc_padtrigger_ec, &b_stgc_padtrigger_ec);
   fChain->SetBranchAddress("stgc_padtrigger_fragid", &stgc_padtrigger_fragid, &b_stgc_padtrigger_fragid);
   fChain->SetBranchAddress("stgc_padtrigger_secid", &stgc_padtrigger_secid, &b_stgc_padtrigger_secid);
   fChain->SetBranchAddress("stgc_padtrigger_spare", &stgc_padtrigger_spare, &b_stgc_padtrigger_spare);
   fChain->SetBranchAddress("stgc_padtrigger_orbit", &stgc_padtrigger_orbit, &b_stgc_padtrigger_orbit);
   fChain->SetBranchAddress("stgc_padtrigger_bcid", &stgc_padtrigger_bcid, &b_stgc_padtrigger_bcid);
   fChain->SetBranchAddress("stgc_padtrigger_l1id", &stgc_padtrigger_l1id, &b_stgc_padtrigger_l1id);
   fChain->SetBranchAddress("stgc_padtrigger_hit_relbcid", &stgc_padtrigger_hit_relbcid, &b_stgc_padtrigger_hit_relbcid);
   fChain->SetBranchAddress("stgc_padtrigger_hit_pfeb", &stgc_padtrigger_hit_pfeb, &b_stgc_padtrigger_hit_pfeb);
   fChain->SetBranchAddress("stgc_padtrigger_hit_tdschannel", &stgc_padtrigger_hit_tdschannel, &b_stgc_padtrigger_hit_tdschannel);
   fChain->SetBranchAddress("stgc_padtrigger_hit_padchannel", &stgc_padtrigger_hit_padchannel, &b_stgc_padtrigger_hit_padchannel);
   fChain->SetBranchAddress("stgc_padtrigger_hit_sourceid", &stgc_padtrigger_hit_sourceid, &b_stgc_padtrigger_hit_sourceid);
   fChain->SetBranchAddress("stgc_padtrigger_pfeb_addr", &stgc_padtrigger_pfeb_addr, &b_stgc_padtrigger_pfeb_addr);
   fChain->SetBranchAddress("stgc_padtrigger_pfeb_nchan", &stgc_padtrigger_pfeb_nchan, &b_stgc_padtrigger_pfeb_nchan);
   fChain->SetBranchAddress("stgc_padtrigger_pfeb_disconnected", &stgc_padtrigger_pfeb_disconnected, &b_stgc_padtrigger_pfeb_disconnected);
   fChain->SetBranchAddress("stgc_padtrigger_pfeb_sourceid", &stgc_padtrigger_pfeb_sourceid, &b_stgc_padtrigger_pfeb_sourceid);
   fChain->SetBranchAddress("stgc_padtrigger_trigger_bandid", &stgc_padtrigger_trigger_bandid, &b_stgc_padtrigger_trigger_bandid);
   fChain->SetBranchAddress("stgc_padtrigger_trigger_phiid", &stgc_padtrigger_trigger_phiid, &b_stgc_padtrigger_trigger_phiid);
   fChain->SetBranchAddress("stgc_padtrigger_trigger_relbcid", &stgc_padtrigger_trigger_relbcid, &b_stgc_padtrigger_trigger_relbcid);
   fChain->SetBranchAddress("stgc_padtrigger_trigger_sourceid", &stgc_padtrigger_trigger_sourceid, &b_stgc_padtrigger_trigger_sourceid);
   fChain->SetBranchAddress("stgc_padtrigger_bcid_rel", &stgc_padtrigger_bcid_rel, &b_stgc_padtrigger_bcid_rel);
   fChain->SetBranchAddress("stgc_padtrigger_bcid_status", &stgc_padtrigger_bcid_status, &b_stgc_padtrigger_bcid_status);
   fChain->SetBranchAddress("stgc_padtrigger_bcid_multzero", &stgc_padtrigger_bcid_multzero, &b_stgc_padtrigger_bcid_multzero);
   fChain->SetBranchAddress("stgc_padtrigger_bcid_sourceid", &stgc_padtrigger_bcid_sourceid, &b_stgc_padtrigger_bcid_sourceid);
   fChain->SetBranchAddress("TGC_coin_x_In", &TGC_coin_x_In, &b_TGC_coin_x_In);
   fChain->SetBranchAddress("TGC_coin_y_In", &TGC_coin_y_In, &b_TGC_coin_y_In);
   fChain->SetBranchAddress("TGC_coin_z_In", &TGC_coin_z_In, &b_TGC_coin_z_In);
   fChain->SetBranchAddress("TGC_coin_x_Out", &TGC_coin_x_Out, &b_TGC_coin_x_Out);
   fChain->SetBranchAddress("TGC_coin_y_Out", &TGC_coin_y_Out, &b_TGC_coin_y_Out);
   fChain->SetBranchAddress("TGC_coin_z_Out", &TGC_coin_z_Out, &b_TGC_coin_z_Out);
   fChain->SetBranchAddress("TGC_coin_width_In", &TGC_coin_width_In, &b_TGC_coin_width_In);
   fChain->SetBranchAddress("TGC_coin_width_Out", &TGC_coin_width_Out, &b_TGC_coin_width_Out);
   fChain->SetBranchAddress("TGC_coin_isAside", &TGC_coin_isAside, &b_TGC_coin_isAside);
   fChain->SetBranchAddress("TGC_coin_isForward", &TGC_coin_isForward, &b_TGC_coin_isForward);
   fChain->SetBranchAddress("TGC_coin_phi", &TGC_coin_phi, &b_TGC_coin_phi);
   fChain->SetBranchAddress("TGC_coin_type", &TGC_coin_type, &b_TGC_coin_type);
   fChain->SetBranchAddress("TGC_coin_isStrip", &TGC_coin_isStrip, &b_TGC_coin_isStrip);
   fChain->SetBranchAddress("TGC_coin_sign", &TGC_coin_sign, &b_TGC_coin_sign);
   fChain->SetBranchAddress("TGC_coin_trackletId", &TGC_coin_trackletId, &b_TGC_coin_trackletId);
   fChain->SetBranchAddress("TGC_coin_trackletIdStrip", &TGC_coin_trackletIdStrip, &b_TGC_coin_trackletIdStrip);
   fChain->SetBranchAddress("TGC_coin_roi", &TGC_coin_roi, &b_TGC_coin_roi);
   fChain->SetBranchAddress("TGC_coin_pt", &TGC_coin_pt, &b_TGC_coin_pt);
   fChain->SetBranchAddress("TGC_coin_coinflag", &TGC_coin_coinflag, &b_TGC_coin_coinflag);
   fChain->SetBranchAddress("TGC_coin_innerflag", &TGC_coin_innerflag, &b_TGC_coin_innerflag);
   fChain->SetBranchAddress("TGC_coin_delta", &TGC_coin_delta, &b_TGC_coin_delta);
   fChain->SetBranchAddress("TGC_coin_sub", &TGC_coin_sub, &b_TGC_coin_sub);
   fChain->SetBranchAddress("TGC_coin_veto", &TGC_coin_veto, &b_TGC_coin_veto);
   fChain->SetBranchAddress("TGC_coin_bunch", &TGC_coin_bunch, &b_TGC_coin_bunch);
   fChain->SetBranchAddress("TGC_coin_inner", &TGC_coin_inner, &b_TGC_coin_inner);
   fChain->SetBranchAddress("TGC_coin_inner_bcid", &TGC_coin_inner_bcid, &b_TGC_coin_inner_bcid);
   fChain->SetBranchAddress("TGC_coin_inner_id", &TGC_coin_inner_id, &b_TGC_coin_inner_id);
   fChain->SetBranchAddress("TGC_coin_inner_eta", &TGC_coin_inner_eta, &b_TGC_coin_inner_eta);
   fChain->SetBranchAddress("TGC_coin_inner_phi", &TGC_coin_inner_phi, &b_TGC_coin_inner_phi);
   fChain->SetBranchAddress("TGC_coin_inner_deta", &TGC_coin_inner_deta, &b_TGC_coin_inner_deta);
   fChain->SetBranchAddress("TGC_coin_inner_dphi", &TGC_coin_inner_dphi, &b_TGC_coin_inner_dphi);
   fChain->SetBranchAddress("TGC_coin_inner_flag", &TGC_coin_inner_flag, &b_TGC_coin_inner_flag);
   fChain->SetBranchAddress("TGC_coin_inner_decision", &TGC_coin_inner_decision, &b_TGC_coin_inner_decision);
   fChain->SetBranchAddress("TGC_coin_inner_nsw_sl", &TGC_coin_inner_nsw_sl, &b_TGC_coin_inner_nsw_sl);
   fChain->SetBranchAddress("trig_L1_mu_eta", &trig_L1_mu_eta, &b_trig_L1_mu_eta);
   fChain->SetBranchAddress("trig_L1_mu_phi", &trig_L1_mu_phi, &b_trig_L1_mu_phi);
   fChain->SetBranchAddress("trig_L1_mu_thrNumber", &trig_L1_mu_thrNumber, &b_trig_L1_mu_thrNumber);
   fChain->SetBranchAddress("trig_L1_mu_RoINumber", &trig_L1_mu_RoINumber, &b_trig_L1_mu_RoINumber);
   fChain->SetBranchAddress("trig_L1_mu_sectorAddress", &trig_L1_mu_sectorAddress, &b_trig_L1_mu_sectorAddress);
   fChain->SetBranchAddress("trig_L1_mu_source", &trig_L1_mu_source, &b_trig_L1_mu_source);
   fChain->SetBranchAddress("trig_L1_mu_hemisphere", &trig_L1_mu_hemisphere, &b_trig_L1_mu_hemisphere);
   fChain->SetBranchAddress("trig_L1_mu_firstCandidate", &trig_L1_mu_firstCandidate, &b_trig_L1_mu_firstCandidate);
   fChain->SetBranchAddress("trig_L1_mu_moreCandInRPCPad", &trig_L1_mu_moreCandInRPCPad, &b_trig_L1_mu_moreCandInRPCPad);
   fChain->SetBranchAddress("trig_L1_mu_moreCandInSector", &trig_L1_mu_moreCandInSector, &b_trig_L1_mu_moreCandInSector);
   fChain->SetBranchAddress("trig_L1_mu_charge", &trig_L1_mu_charge, &b_trig_L1_mu_charge);
   fChain->SetBranchAddress("trig_L1_mu_BWcoin", &trig_L1_mu_BWcoin, &b_trig_L1_mu_BWcoin);
   fChain->SetBranchAddress("trig_L1_mu_InnerCoin", &trig_L1_mu_InnerCoin, &b_trig_L1_mu_InnerCoin);
   fChain->SetBranchAddress("trig_L1_mu_GoodMF", &trig_L1_mu_GoodMF, &b_trig_L1_mu_GoodMF);
   fChain->SetBranchAddress("trig_L1_mu_vetoed", &trig_L1_mu_vetoed, &b_trig_L1_mu_vetoed);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_m", &mu_m, &b_mu_m);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_author", &mu_author, &b_mu_author);
   fChain->SetBranchAddress("mu_allAuthors", &mu_allAuthors, &b_mu_allAuthors);
   fChain->SetBranchAddress("mu_muonType", &mu_muonType, &b_mu_muonType);
   fChain->SetBranchAddress("mu_ptcone20", &mu_ptcone20, &b_mu_ptcone20);
   fChain->SetBranchAddress("mu_ptcone30", &mu_ptcone30, &b_mu_ptcone30);
   fChain->SetBranchAddress("mu_ptcone40", &mu_ptcone40, &b_mu_ptcone40);
   fChain->SetBranchAddress("mu_trackfitchi2", &mu_trackfitchi2, &b_mu_trackfitchi2);
   fChain->SetBranchAddress("mu_trackfitndof", &mu_trackfitndof, &b_mu_trackfitndof);
   fChain->SetBranchAddress("mu_isPassedMCP", &mu_isPassedMCP, &b_mu_isPassedMCP);
   fChain->SetBranchAddress("mu_quality", &mu_quality, &b_mu_quality);
   fChain->SetBranchAddress("mu_ext_targetPlaneVec", &mu_ext_targetPlaneVec, &b_mu_ext_targetPlaneVec);
   fChain->SetBranchAddress("mu_ext_targetEtaVec", &mu_ext_targetEtaVec, &b_mu_ext_targetEtaVec);
   fChain->SetBranchAddress("mu_ext_targetPhiVec", &mu_ext_targetPhiVec, &b_mu_ext_targetPhiVec);
   fChain->SetBranchAddress("mu_ext_targetPxVec", &mu_ext_targetPxVec, &b_mu_ext_targetPxVec);
   fChain->SetBranchAddress("mu_ext_targetPyVec", &mu_ext_targetPyVec, &b_mu_ext_targetPyVec);
   fChain->SetBranchAddress("mu_ext_targetPzVec", &mu_ext_targetPzVec, &b_mu_ext_targetPzVec);
   fChain->SetBranchAddress("trigger_info_chain", &trigger_info_chain, &b_trigger_info_chain);
   fChain->SetBranchAddress("trigger_info_typeVec", &trigger_info_typeVec, &b_trigger_info_typeVec);
   fChain->SetBranchAddress("trigger_info_ptVec", &trigger_info_ptVec, &b_trigger_info_ptVec);
   fChain->SetBranchAddress("trigger_info_etaVec", &trigger_info_etaVec, &b_trigger_info_etaVec);
   fChain->SetBranchAddress("trigger_info_phiVec", &trigger_info_phiVec, &b_trigger_info_phiVec);
   fChain->SetBranchAddress("TILE_murcv_trig_mod", &TILE_murcv_trig_mod, &b_TILE_murcv_trig_mod);
   fChain->SetBranchAddress("TILE_murcv_trig_part", &TILE_murcv_trig_part, &b_TILE_murcv_trig_part);
   fChain->SetBranchAddress("TILE_murcv_trig_bit0", &TILE_murcv_trig_bit0, &b_TILE_murcv_trig_bit0);
   fChain->SetBranchAddress("TILE_murcv_trig_bit1", &TILE_murcv_trig_bit1, &b_TILE_murcv_trig_bit1);
   fChain->SetBranchAddress("TILE_murcv_trig_bit2", &TILE_murcv_trig_bit2, &b_TILE_murcv_trig_bit2);
   fChain->SetBranchAddress("TILE_murcv_trig_bit3", &TILE_murcv_trig_bit3, &b_TILE_murcv_trig_bit3);
   Notify();
}

Bool_t TagAndProbeAnalysisClass::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void TagAndProbeAnalysisClass::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t TagAndProbeAnalysisClass::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef TagAndProbeAnalysisClass_cxx
