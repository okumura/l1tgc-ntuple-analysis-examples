ROOTFLAGS = $(shell root-config --cflags)
ROOTLIBS  = $(shell root-config --libs)

exes=analysis.exe
objs=TagAndProbeAnalysisClass.o AnalysisSkeleton.o

all:$(exes)

%.exe:%.cxx $(objs)
	g++ -o $@ $(objs) $< $(ROOTFLAGS) $(ROOTLIBS)

%.o:%.C %.h
	g++ -c -o $@ $< $(ROOTFLAGS)

clean:
	rm -f $(exes) *~ output.dat output.root
