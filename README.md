# チュートリアル

## 事前準備 (1)
自分のラップトップに
- g++
- root
が入っている状況を作ってください。

## 事前準備 (2)
CERNのgitlabを使いこなせるようになってください。[gitlabの始め方.pdf](/uploads/fc4a89f43cb8a8c205114b6678792468/gitlabの始め方.pdf)を参照。
要点は、
- gitlabのユーザーアカウントを作る
- ローカルのコンピュータにsshkeyを作って、公開鍵をgitlabに登録しておく（そうすると、sshをつかったgit clone等をpassword入力なしにできてハッピー）
かと思います。

## コンパイル
```
mkdir tutorial
cd tutorial
git clone ssh://git@gitlab.cern.ch:7999/okumura/l1tgc-ntuple-analysis-examples.git
cd l1tgc-ntuple-analysis-examples
wget https://www.icepp.s.u-tokyo.ac.jp/~okumura/tmp/user.hasegawa.34022423.EXT0._000067.root .
wget https://www.icepp.s.u-tokyo.ac.jp/~okumura/tmp/user.hasegawa.34022423.EXT0._000068.root .
ls user.hasegawa.*root > input_files.txt # 単純にはinput_files.txtに２つのrootファイル名を2行に渡って書く、というだけの作業。editorでやってもよい。以下のanalysisのときに入力データセットリストとして使っている。
make clean
make
./analysis.exe
```

出力
```
[okumura@YasuyukinoMacBook-Pro l1tgc-ntuple-analysis-examples]$ ./analysis.exe 
0 th event is being processed
1000 th event is being processed
...
76000 th event is being processed
0 th event is being processed
1000 th event is being processed
2000 th event is being processed
...
76000 th event is being processed
Warning in <TGraphAsymmErrors::Divide>: Number of graph points is different than histogram bins - 26 points have been skipped
```

`output.root`と`output_test.root`ができるので中身をみてみよう。
コードと対応させて、どういうヒストグラムが入っているのかを理解してみましょう。

# 簡単な説明
- TagAndProbeAnalysisClsss: いわゆるZ->mumuを使ったTag and Probe (タグ・アンド・プローブ)解析が実装されている。結構高度な解析になっているので、コードを読んでどんなことをやっているのかが
- AnalysisSkeleton: 簡単な解析コード。`TTree`の`MakeClass`関数で作ったものからの変更点は`AnalysisSkeleton.C.orig`と`AnalysisSkeleton.h.orig`との違いをみるとわかる。
- analysis.cxx main関数を持つコード。この人は最上位のコードになっていて、この中でTagAndProbeAnalysisClassクラスと　AnalysisSkeletonクラスのobjectを作ってLoop関数をよんでいる。解析の本体は`TaAndProbeAnalysisClass.C`であったり`AnalysisSkeleton.C`であったりの中身になるので、こっちは解析を回すインフラ的な感じの立ち位置です。

# Know issues
`TagAndProbeAnalysisClass.C`の解決できていない問題点なのですが、コードを理解するという課題の趣旨からは若干外れるのですが、Triggerの情報の入れ方の不具合（もしくは不理解）によって、turn on curveが変で、high pTで効率が下がっているような不自然な結果がでてきます。そのうち理解ができて直せるとよいのだけど今のところそのままにしてあります。
![image](/uploads/d08b60050fb3c2ca0c60e21b60a56ac9/image.png)
